# frozen_string_literal: true

require_relative 'lib/prometheus_exporter/rails_elapsed_time/version'

Gem::Specification.new do |spec|
  spec.name = 'prometheus_exporter-rails_elapsed_time'
  spec.version = PrometheusExporter::RailsElapsedTime::VERSION
  spec.authors = ['Guillaume Hain']
  spec.email = ['zedtux@zedroot.org']
  spec.summary = 'Prometheus exporter for Rails elapsed times'
  spec.description = 'This gem extends the prometheus_exporter gem with Rails' \
                     'elapsed times on view, database and more.'
  spec.homepage = 'https://gitlab.com/pharmony/prometheus_exporter-rails_elapsed_time'
  spec.license = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'prometheus_exporter', '>= 0.7', '< 2'
end
